import java.util.ArrayList;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.LightSensor;
import lejos.nxt.Sound;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;

public class SimpleMoves {

	private static DifferentialPilot pilot;
	private LightSensor lightSen = new LightSensor(SensorPort.S3);
	private static  ArrayList<int[]> wektory = new ArrayList<int[]>();

	private int[][] pola = new int[8][8];
	private static int[][] odwiedzone_pola = new int[3][25];
	private int start = 0;
	private int celX, celY;//, ilePoSkrecie = 0;
	private int[] kierunki;
	private int loop = 0;
	public static int[] path;
	private int startX = 0;
	private int startY = 0;
	private static int[] aktualnaPozycja = new int[2];
	private static int[] poprzedniaPozycja = new int[2]; // potrzebne do stwierdzenia w którym kierunku jedzie
	private static int[] poczatekSciezki = new int[2];
	private static int[] pin =  new int[6];
	public static Boolean skanowanie = true; // steruje skanowaniem (przy pikaniu nie skanuje - na wyswietlaczu pisze , ale i tak nie skanuje, taka zmyła)
	private  static int nrPola; 

	
	private static void cofnijSie(int wsp) throws Exception {
		SimpleMoves robot = new SimpleMoves();
		robot.pilot = new DifferentialPilot(4.0f, 4.0f, Motor.A, Motor.B);
		 pilot.setTravelSpeed(5);
         pilot.travel(wsp*12);
	}
	
	private void stwozMapeOdwiedzonych(int numerWzoru) {
		odwiedzone_pola[0][nrPola] = aktualnaPozycja[1]; //x
		odwiedzone_pola[1][nrPola] = aktualnaPozycja[0]; //y
		odwiedzone_pola[2][nrPola] = numerWzoru;
		System.out.println(odwiedzone_pola[0][nrPola] +" "+ odwiedzone_pola[1][nrPola] +" "+ odwiedzone_pola[2][nrPola]);
		nrPola++;
	}
	
	private void goStraight()  throws Exception{
		 int lightVal = 0;
		 Motor.A.setSpeed((float) 10);
		 Motor.B.setSpeed((float) 10);
		 
         while (true) {
			Motor.B.forward();
			Motor.A.forward();
			Delay.msDelay(4);
                 lightVal = lightSen.readValue();
                 if (lightVal >= 35) {
                         continue;
                 } else {
                        Delay.msDelay(6000);
                         try {
 							int[] wektor = odczyt(0.5);
 							wektory.add(wektor);
 							int numerWzoru = rozpoznajWzor(wektor);
 							if (skanowanie == true) // przy powrocie z pikaniem nie bedzie skanować
 								stwozMapeOdwiedzonych(numerWzoru);
						} catch (Exception e) {
							e.printStackTrace();
						}
                         break;
                 }
         }
         Motor.B.stop();
         Motor.A.stop();
	}
	
	private void goStraightBezSkan()  throws Exception{
		 int lightVal = 0;
		 Motor.A.setSpeed((float) 30);
		 Motor.B.setSpeed((float) 30);
		 Motor.A.forward();
		 Motor.B.forward();
        while (true) {
        		Delay.msDelay(4);
                lightVal = lightSen.readValue();

                if (lightVal >= 35) {
                        continue;
                } else {
                		Delay.msDelay(12000);
                        break;
                }
		}
	}
	
	private void zaMape()  throws Exception{
		pilot.setTravelSpeed(5);
		pilot.travel(6);
		pilot.stop();
	}

	private void goLeft() {
		pilot.setRotateSpeed(50);
		pilot.rotate(200.0, false);
		pilot.stop();
	}

	private void goRight() {
		pilot.setRotateSpeed(50);
		pilot.rotate(-200.0, false);
		pilot.stop();
	}


	private void goBack() throws Exception{// wycofuje- jedzie tylem
		int[] wektor = odczyt(-0.5);
		 wektory.add(wektor);
         int numerWzoru = rozpoznajWzor(wektor);
         if (skanowanie == true)
        	 stwozMapeOdwiedzonych(numerWzoru);
         
         int lightVal = 0;
         Motor.B.setSpeed((float) 10);
   		 Motor.A.setSpeed((float) 11);
    	 Motor.A.backward();
    	 Motor.B.backward();
         while (true) {
        	 	 Delay.msDelay(4);
                 lightVal = lightSen.readValue();

                 if (lightVal >= 35) {
                         continue;
                 } else {
                	 	Delay.msDelay(6000);
                         pilot.stop();               
                         break;
                 }
		}
         Motor.B.stop();
         Motor.A.stop();
	}

	public void moveLikeJagger(int[] path)  throws Exception{
		int loop = 0;
		SimpleMoves robot = new SimpleMoves();
		robot.pilot = new DifferentialPilot(4.0f, 4.0f, Motor.A, Motor.B);
		while (loop < path.length) {
			switch (path[loop]) {
			case 0:
				poprzedniaPozycja[0] = aktualnaPozycja[0];
				aktualnaPozycja[0] += 1;
				robot.goStraight();
				System.out.println("poz "+ aktualnaPozycja[0] + " "+ aktualnaPozycja[1]);
				break;
			case 1:
				robot.goLeft();
				break;
			case 2:
				robot.goRight();
				break;
			case 3:
				poprzedniaPozycja[1] = aktualnaPozycja[1];
				aktualnaPozycja[1] += 1;
				robot.goStraightBezSkan();
				System.out.println("poz "+ aktualnaPozycja[0] + " "+ aktualnaPozycja[1]);
				break;
			case 4:
				robot.zaMape();
				break;
//			case 5:
//				robot.rotate180NextRight();
//				break;
			case 6:
				robot.goBack();
				poprzedniaPozycja[0] = aktualnaPozycja[0];
				aktualnaPozycja[0] -= 1;
				System.out.println("poz "+ aktualnaPozycja[0] + " "+ aktualnaPozycja[1]);
				break;
			}
			loop++;
		}
	}

	public void ustawCel(int x, int y) {
		celX = x;
		celY = y;
		kierunki = new int[40];

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i != 0 || j != 0)
					pola[i][j] = -1;
			}
		}
	}

	public int[] getMoves() {
		path = new int[loop];
		for (int i = 0; i < loop; i++) {
			path[i] = kierunki[i];
		}
		return path;
	}

	void ustawPola() {
		int k = 0;
		for (int z = 4; z >= 1; z--) {
			k++;
			for (int i = celX + z; i >= celX - z; i--) {
				for (int j = celY + z; j >= celY - z; j--) {
					if (j <= 4 && i <= 4 && j >= 0 && i >= 0)
						pola[i][j] = k;
				}
			}
		}
		pola[celX][celY] = 9;
	}

	public void wypiszMape() {
//		for (int j = 4; j >= 0; j--) {
//			for (int i = 4; i >= 0; i--) {
//
//			//	System.out.print(pola[i][j] + " ");
//			//	if (i == 0)
//					//System.out.println();
//
//			}
//		}
		for (int i=0; i<5; i++)
			for (int j=0; j<5; j++)
				System.out.print(pola[i][j] + " ");

	}

	public void wypiszTabliceKierunkow() {
		path = new int[loop];
		for (int i = 0; i < loop; i++) {
			path[i] = kierunki[i];
		}
//		for (int i = 0; i < loop; i++) {
//			//System.out.print(path[i] + ", ");
//
//		}
		loop = 0;

	}

	public void setStart(int x, int y) {
		startX = x;
		startY = y;
	}

	public void znajdzSciezke() throws Exception {
		int temp = -1;
		int[] pom = new int[4];
		for (int i = 0; i < 4; i++)
			pom[i] = 0;
		int a = startX;
		int b = startY;
		int max;

		while (a != celX || b != celY) {

			// prosto.
			pom[0] = pola[a][b + 1];
			// lewo
			pom[1] = pola[a + 1][b];
			// prawo
			if ((a - 1) >= 0)
				pom[2] = pola[a - 1][b];

			// do tylu
			if ((b - 1) >= 0)
				pom[3] = pola[a][b - 1];

			temp = 0;
			max = pom[0];
			for (int i = 1; i <= 3; i++) {
				if (pom[i] > max) {
					max = pom[i];
					temp = i;
				}
			}

			// 0-do przodu, 1-obroc sie w prawo, 3-obroc sie w lewo.
			if (temp == 0) {

				b = b + 1;
				kierunki[loop] = 0;

			}
			if (temp == 1) {
				a = a + 1;
				kierunki[loop] = 1;
				loop++;
				kierunki[loop] = 3;
				loop++;
				kierunki[loop] = 2;
			
//				loop++;
//				kierunki[loop] = 4;
			}
			if (temp == 2) {
				a = a - 1;
				kierunki[loop] = 2;
				loop++;
				kierunki[loop] = 3;
				loop++;
				kierunki[loop] = 1;
//				loop++;
//				kierunki[loop] = 4;

			}

			if (temp == 3) {
				b = b - 1;
				kierunki[loop] = 6;

			}
			pola[a][b] = 0;
			poczatekSciezki[0] = a;
			poczatekSciezki[1] = b;
			loop++;

		}

	}
	
	private int[] odczyt(double odleglosc) throws Exception {
		int[] pomiary = new int[23];
	    int i = 0;
	    int wektor[] = new int[23];
	    double time = 5950;
	    double timeEnd = 0;

	   if (odleglosc > 0) {
		   Motor.A.setSpeed(60);
		   Motor.B.setSpeed((long)60.8);
		   Motor.A.forward();
		   Motor.B.forward();   
	   } else {
		   Motor.A.setSpeed((long)60.8);
		   Motor.B.setSpeed((long)60);
		   Motor.A.backward();
		   Motor.B.backward();
	   }
		   System.out.println("skanuje");

	   int z = 0;
	   while (z < 23) {
		   z++;
		   pomiary[i]=SensorPort.S3.readRawValue();
		   if (pomiary[i]<600) {
		        wektor[i] = 0;
		      } else {
		        wektor[i] = 1;
		      }
		      i++;
			   Delay.msDelay((long)240);
	   }
	   return wektor;
	}

	private int rozpoznajWzor(int[] wektor) {
		int numerWzoru = -1;
		int baza = 0, licznik = 0, iloscJedynek = 0; // bialy
		for (int i : wektor) {
			if (i != baza) {
				licznik++;
				baza = (baza + 1)%2;
			}
			if (i == 1)
				iloscJedynek++;
		}
		
		if (licznik <= 1) {
			if (iloscJedynek >= 5)
			{
				System.out.println("podluzne grube");
				if (aktualnaPozycja[1] == 1 || aktualnaPozycja[1] == 3)
					pilot.travel(-0.9);
				else
					pilot.travel(1.9);
				return 1; // podluzne grube
			}
			else
			{
			System.out.println("puste");
			return 0;
			}
		}

		
		else if (licznik == 2 && iloscJedynek < 4) {
			System.out.println("krzyzyk");
			return 2; // krzyżyk
		}
		
		else if (licznik > 2 && licznik<=4) {
			System.out.println("lezacy grubas");
			if (aktualnaPozycja[1] == 1 || aktualnaPozycja[1] == 3)
				pilot.travel(-0.9);
			else
				pilot.travel(1.9);
			return 3; //lezace grubasy
		}
		else if (licznik > 4) {
			System.out.println("prazki");
			if (aktualnaPozycja[1] == 1 || aktualnaPozycja[1] == 3)
				pilot.travel(-1);
			else
				pilot.travel(2);
			return 4; // prążki
		}
		else {
			System.out.println("nie wiadomo");
			return 9;
		}
	}
	
	private void wyrownajDoLinii() { // po skrecie zeby nie zaczynał skanować od polowy kratki bo wtedy wyjezdza za plansze
		int lightVal = 0;
        pilot.setTravelSpeed(5);
        while (true) {
                pilot.travel(0.25);

                lightVal = lightSen.readValue();

                if (lightVal >= 35) {
                        continue;
                } else {      
                	pilot.travel(-0.5);
                	//pilot.stop();
                        break;
                }
        }
	}
	
	//idz po kolei do wszystkich elementów pinu i zapikaj
	// wyznaczanie startu i celu działa
	// jest problem z chodzeniem od startu do celu (z jakij pozycji zakłąda, że startuje - z jakiego kierunku (N,S,W,E))
	//czy nic nie zakłąda i zawsze myśli że jest przodem
	private void idzDoZnalezionego() throws Exception{
		SimpleMoves sm = new SimpleMoves();
		System.out.println("koniec w fun");
		sm.startX = 4;
		sm.startY = 4;// zaczyna po skonczeniu skanowania -  z ostatniej pozycji
		for (int j=0; j<6; j++) { // dla każdego elementu pinu
				for (int i=0; i<25; i++) { // dla każdrgo wzoru z tabicy odwiedzone_pola
					
						if (odwiedzone_pola[2][i] == pin[j]) {
							System.out.println("koniec w fun 2");
							sm.ustawCel(odwiedzone_pola[0][i], odwiedzone_pola[1][i]);
							System.out.println("cel : "+odwiedzone_pola[0][i] + odwiedzone_pola[1][i]);
							sm.ustawPola();
						    sm.znajdzSciezke();
						    
						    if ((poprzedniaPozycja[0] < aktualnaPozycja[0]) && (poprzedniaPozycja[1] == aktualnaPozycja[1])) {
						    	//goRight();
						    	//ustawiony w lewo
						    	if (poczatekSciezki[0] < aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goLeft();
						    		goLeft();
						    	}	
						    	if (poczatekSciezki[0] > aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
					    		//nic 		
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] < aktualnaPozycja[1])) {
						    		goLeft();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] > aktualnaPozycja[1])) {
						    		goRight();
						    	}	
						    }
						    if ((poprzedniaPozycja[0] > aktualnaPozycja[0]) && (poprzedniaPozycja[1] == aktualnaPozycja[1])) {
						    	//goLeft();
						    	// ustawiony w prawo
						    	if (poczatekSciezki[0] < aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		//nic
						    	}	
						    	if (poczatekSciezki[0] > aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goLeft();
						    		goLeft();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] < aktualnaPozycja[1])) {
						    		goRight();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] > aktualnaPozycja[1])) {
						    		goLeft();
						    	}	

						    }
						    if ((poprzedniaPozycja[0] == aktualnaPozycja[0]) && (poprzedniaPozycja[1] > aktualnaPozycja[1])) {
						    	//goLeft();
						    	//goLeft();
						    	//ustawiony w dol
						    	if (poczatekSciezki[0] < aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goLeft();
						    	}	
						    	if (poczatekSciezki[0] > aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goRight();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] < aktualnaPozycja[1])) {
						    		//nic
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] > aktualnaPozycja[1])) {
						    		goRight();
						    		goRight();
						    	}	
						    }
						    if ((poprzedniaPozycja[0] == aktualnaPozycja[0]) && (poprzedniaPozycja[1] < aktualnaPozycja[1])) {
						    	//goLeft();
						    	//goLeft();
						    	//ustawiony w gore
						    	if (poczatekSciezki[0] < aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goRight();
						    	}	
						    	if (poczatekSciezki[0] > aktualnaPozycja[0] && (poczatekSciezki[1] == aktualnaPozycja[1])) {
						    		goLeft();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] < aktualnaPozycja[1])) {
						    		goRight();
						    		goRight();
						    	}	
						    	if (poczatekSciezki[0] == aktualnaPozycja[0] && (poczatekSciezki[1] > aktualnaPozycja[1])) {
						    		//nic
						    	}	
						    }

						    
						    
							sm.moveLikeJagger(path);
							Sound.beep();
							sm.startX = odwiedzone_pola[0][i];
							sm.startY = odwiedzone_pola[1][i];
							}
					}
//		sm.ustawCel(0, 0); // wróć na początek planszy
//		sm.ustawPola();
//		sm.znajdzSciezke();
//		sm.moveLikeJagger(path);
		Sound.beep(); // na zakonczenie ma zapikac 2 razy
		Sound.beep();
		}
	}
	
	public static void main(String[] args)  throws Exception {
		nrPola = 0;
		for (int i=0; i<3; i++) {
			for (int j=0; j<25; j++) {
				odwiedzone_pola[i][j] = 10;
			}
		}
		aktualnaPozycja[0] = 0;
		aktualnaPozycja[1] = 0;
		poprzedniaPozycja[0] = 0;
		poprzedniaPozycja[1] = 0;
		
		//pin na konkurs
		pin[0] = 1; //podlużne grube
		pin[1] = 2; // krzyzyk
		pin[2] = 0; //puste
		pin[3] = 4; //prazki
		pin[4] = 0; //puste
		pin[5] = 3; //lezacy grubas
		
		
		
		SimpleMoves sm = new SimpleMoves();
		cofnijSie(-1);
		int[] w = sm.odczyt(0.5);
		int numerWzoru = sm.rozpoznajWzor(w);
		sm.stwozMapeOdwiedzonych(numerWzoru);
		sm.setStart(0, 0);
		sm.ustawCel(0, 4);
		sm.ustawPola();
		sm.znajdzSciezke();
		sm.wypiszMape();

		sm.wypiszTabliceKierunkow(); // w gore
		sm.moveLikeJagger(path);

				sm.setStart(0, 4);
				sm.ustawCel(1, 4);
				sm.ustawPola();
				sm.znajdzSciezke();
			 	sm.wypiszMape();

				sm.wypiszTabliceKierunkow(); // w lewo
				sm.moveLikeJagger(path);

				//System.out.println();
				sm.setStart(1, 4);
				sm.ustawCel(1, 0);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				sm.wyrownajDoLinii();
				sm.moveLikeJagger(path); // w dol

				sm.setStart(1, 0);
				cofnijSie(-1);
				w = sm.odczyt(0.5);
			
				numerWzoru = sm.rozpoznajWzor(w);
				sm.stwozMapeOdwiedzonych(numerWzoru);
				sm.wektory.add(w);

				sm.ustawCel(2, 0);

				sm.wektory.add(w);

				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				sm.moveLikeJagger(path); // w lewo
				cofnijSie(-1);
				w = sm.odczyt(0.5);
				numerWzoru = sm.rozpoznajWzor(w);
				sm.stwozMapeOdwiedzonych(numerWzoru);

				sm.setStart(2, 0);
				sm.ustawCel(2, 4);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();
				
				sm.wypiszTabliceKierunkow();
				sm.moveLikeJagger(path); // do gory
				System.out.println();
				sm.setStart(2, 4);
				sm.ustawCel(3, 4);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				sm.moveLikeJagger(path); // w lewo

				sm.setStart(3, 4);
				sm.ustawCel(3, 0);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				sm.wyrownajDoLinii();
				sm.moveLikeJagger(path); // w dol

				sm.setStart(3, 0);
				sm.ustawCel(4, 0);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				cofnijSie(-1);
				w = sm.odczyt(0.5);
				numerWzoru = sm.rozpoznajWzor(w);
				sm.stwozMapeOdwiedzonych(numerWzoru);
				sm.moveLikeJagger(path); // w lewo

				cofnijSie(-1);
				w = sm.odczyt(0.5);
				numerWzoru = sm.rozpoznajWzor(w);
				sm.stwozMapeOdwiedzonych(numerWzoru);
				sm.setStart(4, 0);
				sm.ustawCel(4, 4);
				sm.ustawPola();
				sm.znajdzSciezke();
				sm.wypiszMape();

				sm.wypiszTabliceKierunkow();
				sm.moveLikeJagger(path); // w gore
				
				System.out.println("koniec");
				
				skanowanie = false; // zeby nie nadpisywalo wartosci w mapce
				
				 sm.idzDoZnalezionego();
			
				
	}
	
}
